from django.shortcuts import render

from django.http import HttpResponse


def index(request):
    return HttpResponse("Hola, mundo. Estás en la página de inicio de tu app llamada calc.")

def calcular(request, op, op1, op2):
	if op == "sumar":
		result = op1+op2
		
	elif op == "restar":
		result = op1-op2
		
	elif op == "multiplicar":
		result = op1*op2
		
	elif op == "dividir":
		try:
			result = op1/op2
		except ZeroDivisionError:
			result = "No division by zero allowed!"
			
	else:
		result = "Not valid. Try again!"

	return HttpResponse(result)
